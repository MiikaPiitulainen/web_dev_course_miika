import React from "react";
import Footer from "./footer";
import Header from "./header"
import Card from "./card";
import contacts from "./contacts";

function App() {
  return (
    <div>
      <div>

        <Header />

        <Card
          name={contacts[0].name}
          imgURL={contacts[0].imgURL}
          phone={contacts[0].phone}
          email={contacts[0].email}
        />

        <Card
          name={contacts[1].name}
          imgURL={contacts[1].imgURL}
          phone={contacts[1].phone}
          email={contacts[1].email}
        />

        <Card
          name={contacts[2].name}
          imgURL={contacts[2].imgURL}
          phone={contacts[2].phone}
          email={contacts[2].email}
        />

        <Footer />


      </div>

    </div>
  );
}


export default App;
