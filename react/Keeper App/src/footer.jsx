import React from "react";


const dynamicTime = new Date();
var dynamicYear = dynamicTime.getFullYear();

function Footer() {
  return (

    <footer>
      <p>copyright Miika Piitulainen {dynamicYear}</p>
    </footer>

  );
}

export default Footer;
