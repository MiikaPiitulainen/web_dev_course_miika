import React from "react";

const dateTime = new Date();
var hourTime = dateTime.getHours();
var greeting = "null";

const customStyle = {
  color: ""
};

function whatTime() {
  if (hourTime >= 0 && hourTime <= 16) {
    greeting = "good morning";
    customStyle.color = "red";
  } else if (hourTime > 16 && hourTime <= 20) {
    greeting = "good evening";
    customStyle.color = "green";
  } else {
    greeting = "good night";
    customStyle.color = "blue";
  }
}

function BestGreeting() {
  whatTime();
  return <h1 style ={customStyle}>{greeting}</h1>;
}

export default BestGreeting;
