import React from "react";
import UnorderedList from "./list";
import BestGreeting from "./greeting";

function App() {
  return (
    <div>
      <div>
        <UnorderedList />
        <BestGreeting />
      </div>
    </div>
  );
}

export default App;
